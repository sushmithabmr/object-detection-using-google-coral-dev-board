import argparse

from edgetpu.detection.engine import DetectionEngine
from edgetpu.utils import dataset_utils
from PIL import Image
from PIL import ImageDraw

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('--model', required=True,
      help='Detection SSD model path (must have post-processing operator).')
  parser.add_argument('--label', help='Labels file path.')
  parser.add_argument('--input', help='Input image path.', required=True)
  parser.add_argument('--output', help='Output image path.')
  parser.add_argument('--keep_aspect_ratio', action='store_true',
      help=(
          'keep the image aspect ratio when down-sampling the image by adding '
          'black pixel padding (zeros) on bottom or right. '
          'By default the image is resized and reshaped without cropping. This '
          'option should be the same as what is applied on input images during '
          'model training. Otherwise the accuracy may be affected and the '
          'bounding box of detection result may be stretched.'))
  args = parser.parse_args()

  # Initialize engine.
  engine = DetectionEngine(args.model)
  labels = dataset_utils.read_label_file(args.label) if args.label else None

  # Open image and convert to RGB fromat to process
  img = Image.open(args.input).convert('RGB')
  draw = ImageDraw.Draw(img)

  # Run inference.
  objs = engine.detect_with_image(img,
                                  threshold=0.05,
                                  keep_aspect_ratio=args.keep_aspect_ratio,
                                  relative_coord=False,
                                  top_k=5)

  # Print and draw detected objects.
  for obj in objs:
    print('-----------------------------------------')
    if labels:
      print(labels[obj.label_id])
    print('score =', obj.score)
    box = obj.bounding_box.flatten().tolist()
    print('box =', box)
    draw.rectangle(box, outline='red')

  if not objs:
    print('No objects detected.')

  # Save image with bounding boxes.
  if args.output:
    img.save(args.output)
    print("displaying image")
    img.show()

if __name__ == '__main__':
  main()
